/*
Cycometer A by MDradelt
Measuring the quality of cycle ways / Messung der Qualität von Radwegen

This work is licensed under the Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.

Repository: https://codeberg.org/MDradelt/Cycometer_A

Hardware NodeMCU, NEO-6M, MPU6050 and MicroSD card reader 
setting for Arduino-interface:
-Board: Board: NodeMCU V1 (ESP32-12E Module)
-Upload speed: 115200 baud
*/

#include <TinyGPS++.h>                                    //GPS
#include <SoftwareSerial.h>                               //GPS
#include <Wire.h>                                         //I2C for MPU6050 sensor
#include <SPI.h>                                          //SD card
#include <SD.h>                                           //SD card

int logswitch = D0;                                       //D0=GPIO16 hardware switch logging on/off
long readoutloops = 0;                                    //analyzing the number of sensor readouts for one trackpoint
double accaccu=0;                                         //for calculation of average acceleration

File DataFile;                                            //SD card
const uint8_t pinCS = D8;                                 //SD card: D8 = pin4
boolean logstatus=0;                                      //logstatus 0=no log; 1=log (file header creation in combination with logswitch)
String logfilename="";                                    //name will be created from gps date

const uint8_t scl = D3;                                   //MPU6050 pin setting
const uint8_t sda = D4;                                   //MPU6050 pin setting
const uint8_t MPU6050SlaveAddress = 0x68;                 //MPU6050 Slave Device Address
const uint8_t MPU6050_REGISTER_SMPLRT_DIV   = 0x19;       //MPU6050 configuration register addresses
const uint8_t MPU6050_REGISTER_USER_CTRL    = 0x6A;
const uint8_t MPU6050_REGISTER_PWR_MGMT_1   = 0x6B;
const uint8_t MPU6050_REGISTER_PWR_MGMT_2   = 0x6C;
const uint8_t MPU6050_REGISTER_CONFIG       = 0x1A;
const uint8_t MPU6050_REGISTER_GYRO_CONFIG  = 0x1B;
const uint8_t MPU6050_REGISTER_ACCEL_CONFIG = 0x1C;
const uint8_t MPU6050_REGISTER_FIFO_EN      = 0x23;
const uint8_t MPU6050_REGISTER_INT_ENABLE   = 0x38;
const uint8_t MPU6050_REGISTER_ACCEL_XOUT_H = 0x3B;
const uint8_t MPU6050_REGISTER_SIGNAL_PATH_RESET = 0x68;
const uint16_t AccelScaleFactor = 2048;                   //MPU6050 scale factor responding to g-value from initialisation (2g=16384/4g=8192/8g=4096/16g=2048)-datasheet
//const uint16_t GyroScaleFactor = 131;                   //MPU6050 scale factor for gyrometer
int16_t AccelX, AccelY, AccelZ;                           //MPU6050
double Ax, Ay, Az;                                        //MPU6050
double Axmax, Aymax, Azmax, accmax;                       //maximum acceleraton per trackpoint
//int16_t Temperature, GyroX, GyroY, GyroZ;               //MPU6050 further available values
//double T, Gx, Gy, Gz;                                   //MPU6050 further available values

static const int RXPin = D2, TXPin = D1;                  //GPS
static const uint32_t GPSBaud = 9600;                     //GPS
String Trackpointline ="";                                //data set for one trackpoint
String GPScurrent ="";                                    //store current gps position for later addition to Trackpointline

String Timestamp ="";                                     //GPS time to prevent double trackpoints


TinyGPSPlus gps;                                          // The TinyGPS++ object
SoftwareSerial ss(RXPin, TXPin);                          // The serial connection to the GPS device

void setup(){
  pinMode(logswitch, INPUT);
  Serial.begin(115200);
  Serial.println("Begin of Setup"); 
  ss.begin(GPSBaud); 
  Wire.begin(sda, scl);                                   //MPU6050
  MPU6050_Init();                                         //call subroutine for MPU6050 initialisation
  Serial.print("Initializing SD card...");
  if (!SD.begin(pinCS)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done");                  //end SD card init
  Serial.println("End of Setup");
}

void loop(){
  while (ss.available() > 0){ 
    gps.encode(ss.read());
    if (gps.location.isUpdated() && Timestamp!= String(gps.time.value()) ) {          //when coordinates for next trackpoint are available
     Timestamp = String(gps.time.value());
      Serial.print("sats: "); Serial.println(gps.satellites.value());                 //status info on serial
      Serial.print("Logswitch="); Serial.println(digitalRead(logswitch));             //status info on serial
      if (digitalRead(logswitch)==0){                                                 //no logging when logswitch is off
        logstatus=0;
      }
      if (logstatus==0 and (digitalRead(logswitch)==1)){                              //check if new logfile or track segment has to be created
        long dat=((gps.date.year()*10000)+(gps.date.month())*100+(gps.date.day()));   
        logfilename = (String(dat)+".log");
        Serial.println(logfilename);
        File DataFile = SD.open(logfilename, FILE_WRITE);                             //if file if already exists open to add more tracksegments, otherwise create file
        if (DataFile) {                                                               //if file is available, write to it 
          //DataFile.println("type,date,time,latitude,longitude,alt,speed,shockmax,Axmax,Aymax,Azmax,name"); //add tracksegement header full set
          DataFile.println("type,date,time,latitude,longitude,speed,shockmax,shockavg,name"); //add tracksegement header
          DataFile.close();
          Serial.println("track created in: "+logfilename);
          logstatus=1;                                                                //logstatus 1 for continous logging
        }
        else {
          Serial.println("error opening "+ logfilename);
        }
      }

    //String Trackpointline = (GPScurrent+","+String(accmax)+","+String(Axmax)+","+String(Aymax)+","+String(Azmax)); //create a string for a new trackpoint inlc all axes
    String Trackpointline = (GPScurrent+","+String(accmax))+","+String(accaccu/readoutloops); //create a string for a new trackpoint line in the log file; convert accmax to string data type
    Serial.println(Trackpointline);                                                   // print to the serial port too:
    Serial.println(accaccu);
    
    if (logstatus==1 and (digitalRead(logswitch)==1)){                                //if true then add trackpoint line to log file 
      File DataFile = SD.open(logfilename, FILE_WRITE);                               //open the file  
      if (DataFile) {                                                                 //if the file is available, write to it:
        DataFile.println(Trackpointline);
        DataFile.close();
        Serial.println("TP saved");                                                   //GPScurrent ist empty during the first loop - wait until stable gps-fix
      }
      else {                                                                          //serial error message if file cant be accessed
        Serial.println("error opening "+ logfilename);
      }   
    } 
    else{
      Serial.println("TP not saved");
    }                                                                                 //###########end of log routine

    accmax = 0;                                                                       //reset to 0 = begin new trackpoint
    accaccu=0;
//    Axmax=0;
//    Aymax=0;
//    Azmax=0;
    Serial.print("sensorloops: "); Serial.println(readoutloops);
    readoutloops=0;                                                                   //reset number of acceleration readings to create one trackpoint

    //####Begin new datapoint
    //GPScurrent=("T,"+String((gps.date.year()*10000)+(gps.date.month())*100+(gps.date.day()))+","+String(gps.time.value())+","+String(gps.location.lat(), 6)+","+String(gps.location.lng(),6)+","+String(gps.altitude.meters())+","+String(gps.speed.mps())); 
    GPScurrent=("T,"+String((gps.date.year()*10000)+(gps.date.month())*100+(gps.date.day()))+","+String(gps.time.value())+","+String(gps.location.lat(), 6)+","+String(gps.location.lng(),6)+","+String(gps.speed.mps())); 
    //Serial.println(GPScurrent);
    Serial.println(".");
    }
    else{                                                           //no GPS update -> read sensor again
      readoutloops=readoutloops+1;                                  //number of sensor readouts for analyzing
      readMPU6050data();                                            //get data set from sensor
/*
      Ax=Ax-0.04;                                                   //offset values - possibility of manual calibration
      Ay=Ay-0.02;
      Az=Az-0.94;                                                   //including gravitation for vertical Z-axis; sensor has to be aligned
*/
      double accsum=(sqrt((Ax*Ax)+(Ay*Ay)+(Az*Az)));                //calculate resulting acceleration
      if (accmax<accsum){
        accmax=accsum;                                              //keep highest value
      }
      accaccu=accaccu+accsum;                                       //accumulate value for average calculation
/*
      if (Axmax*Axmax<Ax*Ax){                                       //multiplication to eliminate negative values
        Axmax=Ax;
      }
      if (Aymax*Aymax<Ay*Ay){
        Aymax=(Ay);
      }
      if (Azmax*Azmax<Az*Az){
        Azmax=(Az);
      }        
*/
    }                                                               //end of measuring loop
  }
}                                                                   //end of program main loop


//sub routines-----------------------------------------------------------------------------
void readMPU6050data(){
  Read_RawValue(MPU6050SlaveAddress, MPU6050_REGISTER_ACCEL_XOUT_H);
  Ax = (double)AccelX/AccelScaleFactor;                             //calculate acceleration value with scale factor
  Ay = (double)AccelY/AccelScaleFactor;
  Az = (double)AccelZ/AccelScaleFactor;
  //T = (double)Temperature/340+36.53;                              //calculate temperature value
  //Gx = (double)GyroX/GyroScaleFactor;                             //calculate gyro value with scale factor
  //Gy = (double)GyroY/GyroScaleFactor;
  //Gz = (double)GyroZ/GyroScaleFactor;
/* print values to serial monitor if needed for troubleshooting
  Serial.print("Ax: "); Serial.print(Ax);
  Serial.print(" Ay: "); Serial.print(Ay);
  Serial.print(" Az: "); Serial.print(Az);
  Serial.print(" T: "); Serial.print(T);
  Serial.print(" Gx: "); Serial.print(Gx);
  Serial.print(" Gy: "); Serial.print(Gy);
  Serial.print(" Gz: "); Serial.println(Gz);    
*/
}

void I2C_Write(uint8_t deviceAddress, uint8_t regAddress, uint8_t data){  //MPU6050
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.write(data);
  Wire.endTransmission();
}

void Read_RawValue(uint8_t deviceAddress, uint8_t regAddress){            // read values from MPU6050
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.endTransmission();
  Wire.requestFrom(deviceAddress, (uint8_t)14);
  AccelX = (((int16_t)Wire.read()<<8) | Wire.read());
  AccelY = (((int16_t)Wire.read()<<8) | Wire.read());
  AccelZ = (((int16_t)Wire.read()<<8) | Wire.read());
  //Temperature = (((int16_t)Wire.read()<<8) | Wire.read());             //more possible sensor values
  //GyroX = (((int16_t)Wire.read()<<8) | Wire.read());
  //GyroY = (((int16_t)Wire.read()<<8) | Wire.read());
  //GyroZ = (((int16_t)Wire.read()<<8) | Wire.read());
}

void MPU6050_Init(){                                                      //configure MPU6050
  delay(125);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_SMPLRT_DIV, 0x07);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_PWR_MGMT_1, 0x01);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_PWR_MGMT_2, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_CONFIG, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_GYRO_CONFIG, 0x00);     //set +/-250 degree/second full scale
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_ACCEL_CONFIG, 0x18);    //Set accelerometer full scale range (±2g = 0x00 ±4g=0x08 ±8g=0x10, ±16g = 0x18)
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_FIFO_EN, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_INT_ENABLE, 0x01);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_SIGNAL_PATH_RESET, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_USER_CTRL, 0x00);
}
